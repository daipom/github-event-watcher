# Copyright (C) 2023  Daijiro Fukuda <fukuda@clear-code.com>
# Copyright (C) 2023  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class GitHubPullEventsTest < Test::Unit::TestCase
  Events = Struct.new(:received_webhooks, :watched_paths)

  class Handler < WEBrick::HTTPServlet::AbstractServlet
    include Helper::Fixture

    def initialize(server, events)
      super
      @events = events
    end

    def do_GET(request, response)
      @events.watched_paths << request.path
      case request.path
      when /\A\/repos\/(.+?)\/(.+?)\/events\z/
        owner, repository = Regexp.last_match.to_a[1..2]
      end
      response.status = 200
      response["Content-Type"] = "application/json"
      response.body = fixture_path("events", "#{owner}-#{repository}.json").read
    end

    def do_POST(request, response)
      @events.received_webhooks << JSON.parse(request.body)
      response.status = 200
      response["Content-Type"] = "text/plain"
      response.body = "OK"
    end
  end

  def setup
    Dir.mktmpdir do |tmp_dir|
      @tmp_dir = Pathname(tmp_dir)
      begin
        @bind_address = "127.0.0.1"
        logger = WEBrick::Log.new(nil, WEBrick::Log::WARN)
        http_server = WEBrick::HTTPServer.new(BindAddress: @bind_address,
                                              Port: 0,
                                              Logger: logger,
                                              AccessLog: [])
        @port = http_server[:Port]
        @api_url = "http://#{@bind_address}:#{@port}"
        @xmlua_events = Events.new([], [])
        http_server.mount("/repos/clear-code/xmlua/events", Handler, @xmlua_events)
        @redmine_events = Events.new([], [])
        http_server.mount("/repos/clear-code/redmine_full_text_search/events", Handler, @redmine_events)
        @webhook1_events = Events.new([], [])
        http_server.mount("/webhook1", Handler, @webhook1_events)
        @webhook2_events = Events.new([], [])
        http_server.mount("/webhook2", Handler, @webhook2_events)
        http_server_thread = Thread.new do
          http_server.start
        end
        yield
      ensure
        http_server.shutdown
        http_server_thread&.join
      end
    end
  end

  def actual_webhooks(events, *fetch_values)
    fetch_values = ["number", "action"] if fetch_values.empty?
    events.received_webhooks.collect do |webhook|
      webhook.fetch_values(*fetch_values) { nil }
    end
  end

  def test_command_multiple_end_points
    config = {
      "#{@api_url}/webhook1" => {
        "events" => [
          "PullRequestEvent",
        ],
        "repositories" => [
          {
            "name" => "clear-code/xmlua",
            "api_url" => @api_url,
          },
        ],
      },
      "#{@api_url}/webhook2" => {
        "events" => [
          "PullRequestEvent",
        ],
        "repositories" => [
          {
            "name" => "clear-code/redmine_full_text_search",
            "api_url" => @api_url,
          },
        ],
      },
    }
    run_command(config)
    assert_equal({
                   "webhook1" => [
                     [31, "opened"],
                     [31, "closed"],
                     [33, "opened"],
                     [33, "closed"],
                   ],
                   "webhook2" => [
                     [109, "opened"],
                     [109, "closed"],
                   ],
                 },
                 {
                   "webhook1" => actual_webhooks(@webhook1_events),
                   "webhook2" => actual_webhooks(@webhook2_events),
                 })
  end

  def test_command_multiple_repositories
    config = {
      "#{@api_url}/webhook1" => {
        "events" => [
          "PullRequestEvent",
        ],
        "repositories" => [
          {
            "name" => "clear-code/xmlua",
            "api_url" => @api_url,
          },
          {
            "name" => "clear-code/redmine_full_text_search",
            "api_url" => @api_url,
          },
        ],
      },
    }
    run_command(config)
    assert_equal([
                   [31, "opened"],
                   [31, "closed"],
                   [33, "opened"],
                   [33, "closed"],
                   [109, "opened"],
                   [109, "closed"],
                 ],
                 actual_webhooks(@webhook1_events))
  end

  def test_command_multiple_event_types
    config = {
      "#{@api_url}/webhook1" => {
        "events" => [
          "PullRequestEvent",
          "PushEvent",
        ],
        "repositories" => [
          {
            "name" => "clear-code/redmine_full_text_search",
            "api_url" => @api_url,
          },
        ],
      },
    }
    run_command(config)
    assert_equal([
                   [109, "opened", nil],
                   [nil, nil, "cce915f0dc5ab98838510498087141757dff1dfc"],
                   [nil, nil, "e40241044df37daadd015465aae0e4d3eb0d2aa9"],
                   [nil, nil, "45bda1f5a3117cb982b24676c8d900d43131cdbe"],
                   [nil, nil, "40659ea0141d2b6bd2d95e08c4f76a74dadbc367"],
                   [nil, nil, "8ff1e5e7673ca8891597d1e8694c1473d7dbf01c"],
                   [nil, nil, "cbbe86631715354e7ed70088bfc5251e19a54264"],
                   [109, "closed", nil],
                   [nil, nil, "04a810d3bda5dba652d53cb88fe9038f37878faa"],
                 ],
                 actual_webhooks(@webhook1_events, "number", "action", "after"))
  end

  def test_command_same_repository_for_multiple_webhooks
    config = {
      "#{@api_url}/webhook1" => {
        "events" => [
          "PullRequestEvent",
        ],
        "repositories" => [
          {
            "name" => "clear-code/xmlua",
            "api_url" => @api_url,
          },
        ],
      },
      "#{@api_url}/webhook2" => {
        "events" => [
          "PullRequestEvent",
        ],
        "repositories" => [
          {
            "name" => "clear-code/xmlua",
            "api_url" => @api_url,
          },
        ],
      },
    }
    run_command(config)
    assert_equal({
                   "watched_paths" => ["/repos/clear-code/xmlua/events"],
                   "webhook1" => [
                     [31, "opened"],
                     [31, "closed"],
                     [33, "opened"],
                     [33, "closed"],
                   ],
                   "webhook2" => [
                     [31, "opened"],
                     [31, "closed"],
                     [33, "opened"],
                     [33, "closed"],
                   ],
                 },
                 {
                   "watched_paths" => @xmlua_events.watched_paths,
                   "webhook1" => actual_webhooks(@webhook1_events),
                   "webhook2" => actual_webhooks(@webhook2_events),
                 })
  end

  def run_command(config)
    Dir.chdir(@tmp_dir) do
      config_yaml = @tmp_dir / "config.yaml"
      config_yaml.open("w") do |output|
        output.puts(config.to_yaml)
      end
      command = GitHubEventWatcher::Command::GitHubPullEvents.new
      command.run([
                    "--config", config_yaml.to_s,
                    "--one-shot",
                    "--interval", "0",
                  ])
    end
  end
end
