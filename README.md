# GitHubEventWatcher

This package is for developers who want to watch GitHub repositories
of other people. You can't set
[GitHub webhooks](https://developer.github.com/webhooks/) for GitHub
repositories of other people.

This package pulls events from GitHub and sends GitHub compatible
(subset) POST request to your webhooks.

You can implement commit mail by using with
[GitHub webhooks receiver commit-email/webhooks-mailer](https://gitlab.com/commit-email/webhook-mailer).

## Usage

```console
$ git clone https://gitlab.com/commit-email/github-event-watcher.git
$ cd github-event-watcher
$ cp config.yaml{.example,}
$ editor config.yaml
$ ruby -I lib bin/github-pull-push-events --daemon
```

`config.yaml` uses the following format:

```yaml
"https://webhook.commit-email.example.com/":
  events:
    - PullRequestEvent
    - PushEvent
  repositories:
    - clear-code/redmine_full_text_search
    - ruby/ruby
"https://webhook.zsh.example.com/":
  events:
    - PullRequestEvent
  repositories:
    - clear-code/zsh.d
```

You can add multiple your GitHub webhook receiver URL.
The setting for each are as follows.

* `events`: Specify [GitHub event types](https://docs.github.com/en/developers/webhooks-and-events/events/github-event-types) you want to watch.
  * The following types are supported.
    * `PullRequestEvent`
    * `PushEvent`
* `repositories`: List repositories by `${owner}/${name}` format.

## Authors

* Kouhei Sutou `<kou@clear-code.com>`

## License

GPLv3 or later.
